﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InteractionModes : MonoBehaviour {

	public enum Mode { Build, PlacePerson, PlaceTarget }

	bool placeDestinationMessageSeen = false;
	bool startWalkingMessageSeen = false;

	private Mode m_activeMode = Mode.Build;
	public Mode activeMode
	{
		get
		{
			return m_activeMode;
		}
	}

	public void EnterBuildMode()
	{
		m_activeMode = Mode.Build;
	}

	public void EnterPlacePersonMode()
	{
		m_activeMode = Mode.PlacePerson;
	}

	public void EnterPlaceTargetMode()
	{
		m_activeMode = Mode.PlaceTarget;
		if (!placeDestinationMessageSeen)
		{
			Text tmpText = GameObject.Find("TemporaryText").GetComponent<Text>();
			tmpText.text = "Now click somewhere to place a destination for the person.";
			tmpText.enabled = true;
			placeDestinationMessageSeen = true;
		}
	}

	public void ExitPlaceTargetMode()
	{
		if (!startWalkingMessageSeen)
		{
			Text tmpText = GameObject.Find("TemporaryText").GetComponent<Text>();
			tmpText.text = "Good! Place more people or continue building. When you're done, click 'Start Walking!'.";
			tmpText.enabled = true;
			startWalkingMessageSeen = true;
		}
	}
}
