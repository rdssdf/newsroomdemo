﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (UnityEngine.AI.NavMeshAgent))]
public class PersonBehaviour : MonoBehaviour {

	UnityEngine.AI.NavMeshAgent pathFinder;
	PersonContainer container;
	public Transform target;

	void Start () {
		pathFinder = GetComponent<UnityEngine.AI.NavMeshAgent>();
		container = GameObject.Find("Person Container").GetComponent<PersonContainer>();
		pathFinder.speed = container.personNormalSpeed;
	}
	
	public void StartWalking()
	{
		StartCoroutine(UpdatePath());
	}

	IEnumerator UpdatePath()
	{
		float refreshRate = 0.25f;
		while(target != null)
		{
			// kills performance
			foreach (GameObject person in GameObject.FindGameObjectsWithTag("Person"))
			{
				if (person != gameObject)
				{
					Transform t = person.transform;
					float distance = Vector3.Distance(transform.position, t.position);
					if (distance < container.personPassingThreshold)
						pathFinder.speed = container.personPassingSpeed;
					else
						pathFinder.speed = container.personNormalSpeed;
					//print("distance to others: " + distance + ", speed: " + pathFinder.speed);
				}
			}

			Vector3 targetPosition = new Vector3(target.position.x, 0, target.position.z);
			pathFinder.SetDestination(targetPosition);
			yield return new WaitForSeconds(refreshRate);
		}
	}


}
