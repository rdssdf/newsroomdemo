﻿using UnityEngine;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour {

	public Transform navmeshFloor;
	public Transform tilePrefab;
	public Transform wallPrefab;
	public Vector2 mapSize;
	public Vector2 maxMapSize;
	public float tileSize;
	public Color rawTileColor;
	public Color roomColor;
	public Material tileMaterial;
	[Range(0, 1)]
	public float outlinePercent;
	public float wallHeight = 2f;
	public float wallWidth = 0.25f;



	Transform tilesContainer;
	Transform wallsContainer;
	Dictionary<WallPlacement, Transform> tilesWithWalls = new Dictionary<WallPlacement, Transform>();
	Transform[,] coordsToTile;
	List<HashSet<Coords>> rooms = new List<HashSet<Coords>>();

	public enum Directions { North, East, South, West };

	public struct Coords
	{
		private int m_x;
		public int x
		{
			get
			{
				return m_x;
			}
		}
		private int m_y;
		public int y
		{
			get
			{
				return m_y;
			}
		}

		public Coords(int _x, int _y)
		{
			m_x = _x;
			m_y = _y;
		}

		public override string ToString()
		{
			return "(" + m_x + ", " + m_y + ")";
		}

		public static bool operator ==(Coords a, Coords b)
		{
			return a.Equals(b);
		}

		public static bool operator !=(Coords a, Coords b)
		{
			return !(a == b);
		}

		public override bool Equals(object obj)
		{
			return obj is Coords && Equals((Coords)obj);
		}

		public bool Equals(Coords other)
		{
			return x == other.x && y == other.y;
		}

		public override int GetHashCode()
		{
			return x ^ y;
		}
	}

	struct WallPlacement
	{
		private Coords m_coords;
		public Coords coords
		{
			get
			{
				return m_coords;
			}
		}
		private Directions m_facing;
		public Directions facing
		{
			get
			{
				return m_facing;
			}
		}

		public WallPlacement(Coords _coords, Directions _facing)
		{
			m_coords = _coords;
			m_facing = _facing;
		}

		public static bool operator ==(WallPlacement a, WallPlacement b)
		{
			return a.Equals(b);
		}

		public static bool operator !=(WallPlacement a, WallPlacement b)
		{
			return !(a == b);
		}

		public override bool Equals(object obj)
		{
			return obj is WallPlacement && Equals((WallPlacement)obj);
		}

		public bool Equals(WallPlacement other)
		{
			return coords == other.coords && facing == other.facing;
		}

		public override int GetHashCode()
		{
			return coords.GetHashCode() ^ facing.GetHashCode();
		}
	}

	void Start()
	{
		GenerateMap();
	}

	public void GenerateMap()
	{
		wallsContainer = SetupContainer("Generated Walls");
		tilesContainer = SetupContainer("Generated Tiles");
		GenerateTiles();

		// in the editor this means that modifying any variables used in the formula below (maxMapSize or tileSize) requires to bake the navmesh again
		navmeshFloor.localScale = new Vector3(maxMapSize.x, maxMapSize.y) * tileSize;
	}

	Transform SetupContainer(string containerName)
	{
		if (transform.FindChild(containerName))
		{
			// destroy the object and all of its children so we don't clutter memory and the scene with tons of prefabs
			DestroyImmediate(transform.FindChild(containerName).gameObject);
		}
		Transform result = new GameObject(containerName).transform;
		result.parent = transform;
		return result;
	}

	void GenerateTiles()
	{
		// cleanup datastructures
		coordsToTile = new Transform[(int)mapSize.x, (int)mapSize.y];
		tilesWithWalls.Clear();
		rooms.Clear();

		for (int x = 0; x < mapSize.x; x++)
		{
			for (int y = 0; y < mapSize.y; y++)
			{
				Vector3 tilePosition = CoordToPosition(x, y);
				// use Quaternion to rotate the tile by 90 degrees because by default it faces forward but we want it to face the ground
				Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right * 90)) as Transform;
				newTile.localScale = Vector3.one * (1 - outlinePercent) * tileSize;
				newTile.parent = tilesContainer;
				coordsToTile[x, y] = newTile;

				// assign the material which we set in the editor. this appears to be the only way to change the color of the tile during gameplay
				MeshRenderer tileMesh = newTile.GetComponent<MeshRenderer>();
				tileMesh.material = tileMaterial;
				tileMaterial.color = rawTileColor;
			}
		}
	}

	public void AddRoom(Transform tile)
	{
		Debug.Log("start build room. existing rooms: " + rooms.Count);

		Coords coords = PositionToCoords(tile.position);
		HashSet<HashSet<Coords>> connectedRooms = new HashSet<HashSet<Coords>>();
		List<WallPlacement> wallsToDestroy = new List<WallPlacement>();

		foreach (HashSet<Coords> room in rooms)
		{
			// prevent making the same tile into a room twice
			if (room.Contains(coords))
			{
				Debug.Log("tile is already part of a room. do nothing. existing rooms: " + rooms.Count);
				return;
			}

			// collect all rooms to be connected by the new tile as well as existing walls to be destroyed
			foreach (Coords roomTile in room)
			{
				Directions fromExistingToNew;
				if (IsNeighbor(coords, roomTile, out fromExistingToNew))
				{
					wallsToDestroy.Add(new WallPlacement(roomTile, fromExistingToNew));
					connectedRooms.Add(room);
				}
			}
		}

		Debug.Log("tile connects with " + connectedRooms.Count + " existing rooms. total existing rooms: " + rooms.Count);

		// a new room was created
		if (connectedRooms.Count == 0)
		{
			InstantiateWall(tile, Directions.North);
			InstantiateWall(tile, Directions.South);
			InstantiateWall(tile, Directions.East);
			InstantiateWall(tile, Directions.West);

			HashSet<Coords> newRoom = new HashSet<Coords>();
			newRoom.Add(coords);
			rooms.Add(newRoom);
		}
		else // the new tile connects with one or more existing rooms
		{
			HashSet<Coords> mainRoom = MergeRooms(connectedRooms);
			mainRoom.Add(coords);

			// destroy existing walls between the new tile and the existing rooms
			foreach (WallPlacement placement in wallsToDestroy)
			{
				Transform wallToDestroy;
				if (tilesWithWalls.TryGetValue(placement, out wallToDestroy))
				{
					Destroy(wallToDestroy.gameObject);
					tilesWithWalls.Remove(placement);
				}
			}

			// build walls only on those sides of the tile that are not adjacent to the existing room
			Coords neighbor;
			if (!(TryGetNeighborTile(coords, Directions.North, out neighbor) && mainRoom.Contains(neighbor)))
				InstantiateWall(tile, Directions.North);
			if (!(TryGetNeighborTile(coords, Directions.South, out neighbor) && mainRoom.Contains(neighbor)))
				InstantiateWall(tile, Directions.South);
			if (!(TryGetNeighborTile(coords, Directions.West, out neighbor) && mainRoom.Contains(neighbor)))
				InstantiateWall(tile, Directions.West);
			if (!(TryGetNeighborTile(coords, Directions.East, out neighbor) && mainRoom.Contains(neighbor)))
				InstantiateWall(tile, Directions.East);
		}

		Debug.Log("finish build room. existing rooms: " + rooms.Count);
	}

	Transform InstantiateWall(Transform tile, Directions facing)
	{
		Vector3 position;
		Quaternion rotation;
		float positionY = tile.position.y + wallHeight / 2;

		switch (facing)
		{
			case Directions.North:
				position = new Vector3(tile.position.x, positionY, tile.position.z + (tileSize / 2));
				rotation = Quaternion.identity;
				break;
			case Directions.South:
				position = new Vector3(tile.position.x, positionY, tile.position.z - (tileSize / 2));
				rotation = Quaternion.identity;
				break;
			case Directions.East:
				position = new Vector3(tile.position.x + (tileSize / 2), positionY, tile.position.z);
				rotation = Quaternion.Euler(Vector3.up*90);
				break;
			case Directions.West:
				position = new Vector3(tile.position.x - (tileSize / 2), positionY, tile.position.z);
				rotation = Quaternion.Euler(Vector3.up * 90);
				break;
			default:
				throw new System.ArgumentException("Invalid facing");
		}

		Transform result = Instantiate(wallPrefab, position, rotation) as Transform;
		result.localScale = new Vector3(tileSize, wallHeight, wallWidth);
		result.parent = wallsContainer;

		// remember where we already put walls so we can delete them when we connect rooms
		WallPlacement placement = new WallPlacement(PositionToCoords(tile.position), facing);
		if (!tilesWithWalls.ContainsKey(placement))
			tilesWithWalls.Add(placement, result);

		return result;
	}

	// ATTENTION: Neighbor is undefined if the method returns false!
	bool TryGetNeighborTile(Coords coords, Directions facing, out Coords neighbor)
	{
		if ((facing == Directions.North && coords.y >= mapSize.y - 1) ||
			(facing == Directions.South && coords.y <= 0) ||
			(facing == Directions.West  && coords.x <= 0) ||
			(facing == Directions.East  && coords.x >= mapSize.x - 1))
		{
			neighbor = new Coords(0, 0);
			return false;
		}
		else
		{
			int x = coords.x;
			int y = coords.y;
			switch (facing)
			{
				case Directions.North:
					y++;
					break;
				case Directions.South:
					y--;
					break;
				case Directions.East:
					x++;
					break;
				case Directions.West:
					x--;
					break;
				default:
					throw new System.ArgumentException("Invalid facing");
			}
			neighbor = new Coords(x, y);
			return true;
		}
	}

	// returns the neighboring tile in the specified direction or null if the neighbor would be out of the map bounds
	public Transform GetNeighborTile(Transform tile, Directions facing)
	{
		Coords coords = PositionToCoords(tile.position);
		Coords neighbor;
		if (TryGetNeighborTile(coords, facing, out neighbor))
			return coordsToTile[neighbor.x, neighbor.y];
		else
			return null;
	}

	// returns true if the tiles are adjacent and returns the direction of assumedNeighbor from pointOfView
	// ATTENTION: The direction is undefined if the method returns false!
	bool IsNeighbor(Coords assumedNeigbor, Coords pointOfView, out Directions directionOfNeighbor)
	{
		Coords actualNeighbor;
		if (TryGetNeighborTile(pointOfView, Directions.North, out actualNeighbor) && actualNeighbor == assumedNeigbor)
		{
			directionOfNeighbor = Directions.North;
			return true;
		}
		else if (TryGetNeighborTile(pointOfView, Directions.South, out actualNeighbor) && actualNeighbor == assumedNeigbor)
		{
			directionOfNeighbor = Directions.South;
			return true;
		}
		else if (TryGetNeighborTile(pointOfView, Directions.West, out actualNeighbor) && actualNeighbor == assumedNeigbor)
		{
			directionOfNeighbor = Directions.West;
			return true;
		}
		else if (TryGetNeighborTile(pointOfView, Directions.East, out actualNeighbor) && actualNeighbor == assumedNeigbor)
		{
			directionOfNeighbor = Directions.East;
			return true;
		}
		directionOfNeighbor = Directions.North; // in this case the value is meaningless but we must initialize it nonetheless
		return false;
	}


	HashSet<Coords> MergeRooms(HashSet<HashSet<Coords>> _rooms)
	{
		Debug.Assert(_rooms.Count >= 1);

		// a little optimisation first: be sure to merge into the biggest room
		int maximum = 0;
		// this will not be a valid item yet because MoveNext must be called first, but it's ok, it will be overwritten anyway
		HashSet<Coords> mainRoom = _rooms.GetEnumerator().Current;
		foreach(var x in _rooms)
		{
			if (x.Count > maximum)
				mainRoom = x;
		}

		foreach(HashSet<Coords> room in _rooms)
		{
			if(!room.Equals(mainRoom))
			{
				foreach(Coords coord in room)
					mainRoom.Add(coord);

				rooms.Remove(room);
			}
		}

		return mainRoom;
	}

	// returns the tile enclosing the point or null if the point is out of the map
	public Transform FindTile(Vector3 point)
	{
		// very simple lookup: iterate over all tiles and see which one encloses the point. may be improved using PositionToCoords?
		for (int i = 0; i < tilesContainer.childCount; i++)
		{
			Transform tile = tilesContainer.GetChild(i);
			float halfSize = tileSize / 2;
			if (tile.position.x - halfSize <= point.x && tile.position.x + halfSize > point.x &&
				tile.position.z - halfSize <= point.z && tile.position.z + halfSize > point.z)
			{
				return tile;
			}
		}
		return null;
	}

	public Vector3 CoordToPosition(int x, int y)
	{
		return new Vector3(x - mapSize.x / 2 + 0.5f, 0, y - mapSize.y / 2 + 0.5f) * tileSize;
	}

	public Coords PositionToCoords(Vector3 position)
	{
		Vector3 singleUnit = position / tileSize;
		singleUnit.x += mapSize.x / 2f - 0.5f;
		singleUnit.z += mapSize.y / 2f - 0.5f;
		return new Coords((int)singleUnit.x, (int)singleUnit.z);
	}

}
