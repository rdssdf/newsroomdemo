﻿using UnityEngine;
using System.Collections;

public class WorldCamera : MonoBehaviour {

	float cameraMoveSpeed = 60.0f;

	// TODO try fixed update!
	void Update () {
		if (AreCameraKeysPressed())
		{
			this.transform.Translate(GetDesiredTranslation());
		}	
	}

	Vector3 GetDesiredTranslation()
	{
		float desiredX = 0f;
		float desiredZ = 0f;
		float speed = cameraMoveSpeed * Time.deltaTime; // TODO try fixed delta time

		if (Input.GetKey(KeyCode.W))
			desiredZ = speed;
		if (Input.GetKey(KeyCode.S))
			desiredZ = speed * (-1);
		if (Input.GetKey(KeyCode.A))
			desiredX = speed * (-1);
		if (Input.GetKey(KeyCode.D))
			desiredX = speed;

		float desiredY = Input.GetAxis("Mouse ScrollWheel") * speed * 16f;

		return new Vector3(desiredX, desiredZ, desiredY);
	}

	public static bool AreCameraKeysPressed()
	{
		return Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)
			|| Input.GetAxis("Mouse ScrollWheel") != 0;
	}



}
