﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PersonContainer : MonoBehaviour {

	public Transform personPrefab;
	public Transform cookiePrefab;
	public float personHeight;
	public float cookieRadius;
	public float personNormalSpeed;
	public float personPassingSpeed;
	public float personPassingThreshold;

	bool noteWalkSpeedMessageSeen = false;

	private Transform m_lastPersonPlaced;
	public Transform lastPersonPlaced
	{
		get
		{
			return m_lastPersonPlaced;
		}
	}

	public Transform InstantiatePerson(Vector3 position, Color color)
	{
		Transform result = Instantiate(personPrefab, position, Quaternion.identity) as Transform;
		result.localScale = new Vector3(1f, personHeight, 1f);
		result.GetComponent<MeshRenderer>().material.color = color;
		m_lastPersonPlaced = result;
		result.parent = transform;
		return result;
	}

	public Transform InstantiateCookie(Vector3 position, Color color)
	{
		Transform result = Instantiate(cookiePrefab, position, Quaternion.identity) as Transform;
		result.localScale = new Vector3(cookieRadius, cookieRadius, cookieRadius);
		result.GetComponent<MeshRenderer>().material.color = color;
		result.parent = transform;
		return result;
	}

	public void SetWalking(bool value)
	{
		foreach(GameObject person in GameObject.FindGameObjectsWithTag("Person"))
		{
			PersonBehaviour behaviour = person.GetComponent<PersonBehaviour>();
			behaviour.StartWalking();
		}
		if (!noteWalkSpeedMessageSeen)
		{
			Text tmpText = GameObject.Find("TemporaryText").GetComponent<Text>();
			tmpText.text = "The people will now walk towards their targets. Note how their speed decreases when they pass each other in close range ;)";
			tmpText.enabled = true;
			noteWalkSpeedMessageSeen = true;
		}
	}

}
