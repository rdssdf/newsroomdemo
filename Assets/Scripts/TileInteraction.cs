﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class TileInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
	public Color mouseOverColor;
	public int personColorSeed = 1;

	Color originalColor;
	MapGenerator map;
	MeshRenderer tileRenderer;
	InteractionModes mode;
	PersonContainer container;



	void Start()
	{
		map = GameObject.Find("Map").GetComponent<MapGenerator>();
		mode = GameObject.Find("Interaction Mode").GetComponent<InteractionModes>();
		container = GameObject.Find("Person Container").GetComponent<PersonContainer>();
		tileRenderer = GetComponent<MeshRenderer>();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if(eventData.button == PointerEventData.InputButton.Left)
		{
			if (mode.activeMode == InteractionModes.Mode.Build)
			{
				map.AddRoom(transform);
				originalColor = map.roomColor;
			}
			else if (mode.activeMode == InteractionModes.Mode.PlacePerson)
			{
				Vector3 position = new Vector3(transform.position.x, transform.position.y + container.personHeight, transform.position.z);
				container.InstantiatePerson(position, RandomColor());
				mode.EnterPlaceTargetMode();
				foreach(GameObject button in GameObject.FindGameObjectsWithTag("Button"))
					button.GetComponent<Button>().interactable = false;
			}
			else if(mode.activeMode == InteractionModes.Mode.PlaceTarget)
			{
				Vector3 position = new Vector3(transform.position.x, transform.position.y + container.cookieRadius, transform.position.z);
				Transform cookie = container.InstantiateCookie(position, container.lastPersonPlaced.GetComponent<MeshRenderer>().material.color);
				container.lastPersonPlaced.GetComponent<PersonBehaviour>().target = cookie;
				mode.EnterPlacePersonMode();
				mode.ExitPlaceTargetMode();
				foreach (GameObject button in GameObject.FindGameObjectsWithTag("Button"))
					button.GetComponent<Button>().interactable = true;

			}
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		originalColor = tileRenderer.material.color;
		tileRenderer.material.color = mouseOverColor;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		tileRenderer.material.color = originalColor;
	}

	Color RandomColor()
	{
		return new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
	}

	string PrintNeighbors()
	{
		string result = "";
		Transform north = map.GetNeighborTile(transform, MapGenerator.Directions.North);
		Transform south = map.GetNeighborTile(transform, MapGenerator.Directions.South);
		Transform east = map.GetNeighborTile(transform, MapGenerator.Directions.East);
		Transform west = map.GetNeighborTile(transform, MapGenerator.Directions.West);
		result += "north: " + (north ? map.PositionToCoords(north.position).ToString() : "none");
		result += ", south: " + (south ? map.PositionToCoords(south.position).ToString() : "none");
		result += ", east: " + (east ? map.PositionToCoords(east.position).ToString() : "none");
		result += ", west: " + (west ? map.PositionToCoords(west.position).ToString() : "none");
		return result;
	}

}
