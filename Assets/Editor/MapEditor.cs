﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(MapGenerator))]
public class MapEditor : Editor {

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		// get a reference to our map generator script
		MapGenerator map = target as MapGenerator;

		map.GenerateMap();
	}
}
